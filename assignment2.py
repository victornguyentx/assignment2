from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]


@app.route('/book/JSON')
def bookJSON():
    r = json.dumps(books)
    return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    idList = []
    if request.method == 'POST':
        newName = request.form['name']
        for i in books:
            idList.append(int(i.get("id")))
        newId = max(idList) + 1
        books.append({"title": newName, "id": newId})
    return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        newName = request.form['name']
        for i in books:
            if i.get("id") == str(book_id):
                i["title"] = newName
    return render_template("editBook.html", book_id = book_id, books = books)

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        for i in books:
            if i.get("id") == str(book_id):
                books.remove(i)
    return render_template('deleteBook.html', book_id=book_id, books=books)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)

